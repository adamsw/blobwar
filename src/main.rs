extern crate blobwar;
//use blobwar::board::Board;
use blobwar::configuration::Configuration;
use blobwar::strategy::{Greedy, GreedyPar, AlphaBetaPar, PvsSort, AlphaBetaParSort, AlphaBeta, MinMax, Human, Pvs, Hash};

fn main() {
    //let board = Board::load("x").expect("failed loading board");
    let board = Default::default();
    let mut game = Configuration::new(&board);
    game.battle(PvsSort::new(4),AlphaBetaParSort(4));
}
