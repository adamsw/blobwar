//! Alpha - Beta algorithm.
use itertools::Itertools;
use std::cmp::{max, min};
use std::fmt;

use super::Strategy;
use crate::configuration::{Configuration, Movement};
use crate::shmem::AtomicMove;
use rayon::prelude::*;

/// Anytime alpha beta algorithm.
/// Any time algorithms will compute until a deadline is hit and the process is killed.
/// They are therefore run in another process and communicate through shared memory.
/// This function is intended to be called from blobwar_iterative_deepening.
pub fn alpha_beta_anytime(state: &Configuration) {
    let mut movement = AtomicMove::connect().expect("failed connecting to shmem");
    for depth in 1..100 {
        let chosen_movement = AlphaBetaPar(depth).compute_next_move(state);
        movement.store(chosen_movement);
    }
}

/// Anytime alpha beta par algorithm.
pub fn alpha_beta_par_anytime(state: &Configuration) {
    let mut movement = AtomicMove::connect().expect("failed connecting to shmem");
    for depth in 1..100 {
        let chosen_movement = AlphaBetaPar(depth).compute_next_move(state);
        movement.store(chosen_movement);
    }
}

/// Anytime alpha beta par sort algorithm.
pub fn alpha_beta_par_sort_anytime(state: &Configuration) {
    let mut movement = AtomicMove::connect().expect("failed connecting to shmem");
    for depth in 1..100 {
        let chosen_movement = AlphaBetaParSort(depth).compute_next_move(state);
        movement.store(chosen_movement);
    }
}

/// Alpha - Beta algorithm with given maximum number of recursions.
pub struct AlphaBeta(pub u8);

impl fmt::Display for AlphaBeta {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Alpha - Beta (max level: {})", self.0)
    }
}

impl Strategy for AlphaBeta {
    fn compute_next_move(&mut self, state: &Configuration) -> Option<Movement> {
        decision_alpha_beta(state, self.0)
    }
}

/// Alpha - Beta algorithm with given maximum number of recursions par
pub struct AlphaBetaPar(pub u8);

impl fmt::Display for AlphaBetaPar {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Alpha - Beta Par (max level: {})", self.0)
    }
}

impl Strategy for AlphaBetaPar {
    fn compute_next_move(&mut self, state: &Configuration) -> Option<Movement> {
        decision_alpha_beta_par(state, self.0)
    }
}

/// Alpha - Beta algorithm with given maximum number of recursions with sort and par
pub struct AlphaBetaParSort(pub u8);

impl fmt::Display for AlphaBetaParSort {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Alpha - Beta Par Sort (max level: {})", self.0)
    }
}

impl Strategy for AlphaBetaParSort {
    fn compute_next_move(&mut self, state: &Configuration) -> Option<Movement> {
        decision_alpha_beta_par_sort(state, self.0)
    }
}

/// Decision version par
pub fn decision_alpha_beta_par(state: &Configuration, depth: u8) -> Option<Movement> {
    let movements: Vec<(i8, Option<Movement>)> = state
        .movements()
        .collect_vec()
        .into_par_iter()
        .map(|mvt| -> (i8, Option<Movement>) {
            let new_value = value_min_alpha_beta(&state.play(&mvt), depth - 1, -64, 64);
            (new_value, Some(mvt))
        })
        .collect();
    if movements.is_empty() {
        return None;
    }
    movements
        .into_par_iter()
        .max_by_key(|pair| pair.0)
        .unwrap()
        .1
}

/// Decision version par sort by greedy values
pub fn decision_alpha_beta_par_sort(state: &Configuration, depth: u8) -> Option<Movement> {
    let mut sorted_movements: Vec<Movement> = state.movements().collect_vec();
    sorted_movements.par_sort_by_key(|mvt| state.play(mvt).value());
    let movements: Vec<(i8, Option<Movement>)> = sorted_movements
        .into_par_iter()
        .map(|mvt| -> (i8, Option<Movement>) {
            let new_value = value_min_alpha_beta_sort(&state.play(&mvt), depth - 1, -64, 64);
            (new_value, Some(mvt))
        })
        .collect();
    if movements.is_empty() {
        return None;
    }
    movements
        .into_par_iter()
        .max_by_key(|pair| pair.0)
        .unwrap()
        .1
}

/// Decision version seq
pub fn decision_alpha_beta(state: &Configuration, depth: u8) -> Option<Movement> {
    let mut value: i8 = -64;
    let mut best_mvt: Option<Movement> = None;
    for mvt in state.movements() {
        let new_value = value_min_alpha_beta(&state.play(&mvt), depth - 1, -64, 64);
        if new_value >= value {
            value = new_value;
            best_mvt = Some(mvt);
        }
    }
    best_mvt
}

/// ValueMin
pub fn value_min_alpha_beta(state: &Configuration, depth: u8, alpha: i8, mut beta: i8) -> i8 {
    if depth == 0 || state.movements().next().is_none() {
        return state.value();
    }
    let mut value: i8 = 64;
    for mvt in state.movements() {
        value = min(
            value,
            value_max_alpha_beta(&state.play(&mvt), depth - 1, alpha, beta),
        );
        if value <= alpha {
            break;
        }
        beta = min(beta, value);
    }
    value
}

/// ValueMax
pub fn value_max_alpha_beta(state: &Configuration, depth: u8, mut alpha: i8, beta: i8) -> i8 {
    if depth == 0 || state.movements().next().is_none() {
        return -state.value();
    }
    let mut value: i8 = -64;
    for mvt in state.movements() {
        value = max(
            value,
            value_min_alpha_beta(&state.play(&mvt), depth - 1, alpha, beta),
        );
        if value >= beta {
            break;
        }
        alpha = max(alpha, value);
    }
    value
}

/// ValueMin Sort
pub fn value_min_alpha_beta_sort(state: &Configuration, depth: u8, alpha: i8, mut beta: i8) -> i8 {
    if depth == 0 || state.movements().next().is_none() {
        state.value()
    } else {
        let mut value: i8 = 64;
        for mvt in state.movements() {
            value = min(
                value,
                value_max_alpha_beta(&state.play(&mvt), depth - 1, alpha, beta),
            );
            if value <= alpha {
                break;
            }
            beta = min(beta, value);
        }
        value
    }
}

/// ValueMax Sort
pub fn value_max_alpha_beta_sort(state: &Configuration, depth: u8, mut alpha: i8, beta: i8) -> i8 {
    if depth == 0 || state.movements().next().is_none() {
        -state.value()
    } else {
        let mut value: i8 = -64;
        for mvt in state.movements() {
            value = max(
                value,
                value_min_alpha_beta(&state.play(&mvt), depth - 1, alpha, beta),
            );
            if value >= beta {
                break;
            }
            alpha = max(alpha, value);
        }
        value
    }
}
