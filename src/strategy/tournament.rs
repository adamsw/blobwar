//! Tournament algorithm based on alpha beta.
use std::borrow::BorrowMut;
use std::cmp::{max, min};
use std::fmt;

use super::Strategy;
use crate::configuration::{Configuration, Movement};
use crate::shmem::AtomicMove;
use itertools::Itertools;
use rayon::prelude::{IntoParallelIterator, ParallelIterator, ParallelSliceMut};
use std::collections::HashMap;

/// Anytime Tournament algorithm.
/// Any time algorithms will compute until a deadline is hit and the process is killed.
/// They are therefore run in another process and communicate through shared memory.
/// This function is intended to be called from blobwar_iterative_deepening.

/// Pvs anytime sort
pub fn pvs_sort_anytime(state: &Configuration) {
    let mut movement = AtomicMove::connect().expect("failed connecting to shmem");
    for depth in 3..100 {
        let chosen_movement = PvsSort::new(depth).compute_next_move(state);
        movement.store(chosen_movement);
    }
}

impl PvsSort {
    /// Constructor
    pub fn new(depth: u8) -> Self {
        PvsSort(depth, 0.75)
    }
}

/// Pvs algorithm with given maximum number of recursions.
pub struct PvsSort(pub u8, pub f32);

impl fmt::Display for PvsSort {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Principal variation search sort (max level: {})", self.0)
    }
}

impl Strategy for PvsSort {
    fn compute_next_move(&mut self, state: &Configuration) -> Option<Movement> {
        decision_principal_variation_search_sort(state, self.0, self.1)
    }
}

/// Decision principal variation search sort
pub fn decision_principal_variation_search_sort(
    state: &Configuration,
    depth: u8,
    percent_cut: f32,
) -> Option<Movement> {
    let mut sorted_movements: Vec<Movement> = state.movements().collect_vec();
    sorted_movements.par_sort_by_key(|mvt| -state.play(mvt).value());

    let moves: &[Movement];
    if sorted_movements.len() < 10{
        moves = &sorted_movements[0..sorted_movements.len()];
    }
    else {
        moves = &sorted_movements[0..(sorted_movements.len() as f32 * percent_cut) as usize];
    }

    let movements: Vec<(i8, Option<Movement>)> =  moves
        .into_par_iter()
        .map(|mvt| -> (i8, Option<Movement>) {
            let new_value = -principal_variation_search_sort(&state.play(mvt), depth - 1, -64, 64);
            (new_value, Some(*mvt))
        })
        .collect();
    if movements.is_empty() {
        return None;
    }
    movements
        .into_par_iter()
        .max_by_key(|pair| pair.0)
        .unwrap()
        .1
}

/// Principal variation search sort
pub fn principal_variation_search_sort(
    state: &Configuration,
    depth: u8,
    mut alpha: i8,
    beta: i8,
) -> i8 {
    if depth == 0 || state.movements().next().is_none() {
        return -state.value();
    }

    let mut it = state.movements();
    let first: Movement = it.next().unwrap();
    let mut value: i8 = -principal_variation_search(&state.play(&first), depth - 1, -beta, -alpha);
    alpha = max(alpha, value);
    if alpha >= beta {
        return alpha;
    }

    for mvt in it {
        let next = &state.play(&mvt);
        value = -principal_variation_search_sort(next, depth - 1, -alpha - 1, -alpha);
        if alpha < value && value < beta {
            value = -principal_variation_search_sort(next, depth - 1, -beta, -value);
        }
        alpha = max(alpha, value);
        if alpha >= beta {
            break;
        }
    }
    alpha
}

/// Hash algorithm with given maximum number of recursions.
pub struct Hash(pub u8);

impl Hash {
    /// Constructor
    pub fn new(depth: u8) -> Self {
        Hash(depth)
    }
}

impl fmt::Display for Hash {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Hash (max level: {})", self.0)
    }
}

impl Strategy for Hash {
    fn compute_next_move(&mut self, state: &Configuration) -> Option<Movement> {
        decision_alpha_beta_hash(state, self.0, HashMap::new().borrow_mut())
    }
}

/// Hash anytime
pub fn hash_anytime(state: &Configuration) {
    let mut movement = AtomicMove::connect().expect("failed connecting to shmem");
    for depth in 1..100 {
        let chosen_movement = Hash::new(depth).compute_next_move(state);
        movement.store(chosen_movement);
    }
}

/// Decision version with hash
pub fn decision_alpha_beta_hash(
    state: &Configuration,
    depth: u8,
    table: &mut HashMap<String, (i8, i8)>,
) -> Option<Movement> {
    let mut value: i8 = -64;
    let mut best_mvt: Option<Movement> = None;
    for mvt in state.movements() {
        let new_value = value_min_alpha_beta_hash(&state.play(&mvt), depth - 1, -64, 64, table);
        if new_value >= value {
            value = new_value;
            best_mvt = Some(mvt);
        }
    }
    best_mvt
}

/// ValueMin with hash
pub fn value_min_alpha_beta_hash(
    state: &Configuration,
    depth: u8,
    alpha: i8,
    mut beta: i8,
    table: &mut HashMap<String, (i8, i8)>,
) -> i8 {
    if depth == 0 || state.movements().next().is_none() {
        return state.value();
    }

    let key = state.serialize();
    if table.contains_key(&key) {
        let value = table.get(&key).unwrap();
        if value.0 >= depth as i8 {
            return value.1;
        }
    }

    let mut value: i8 = 64;
    for mvt in state.movements() {
        value = min(
            value,
            value_max_alpha_beta_hash(&state.play(&mvt), depth - 1, alpha, beta, table),
        );
        if value <= alpha {
            break;
        }
        beta = min(beta, value);
    }
    let new_entry = table.entry(key).or_insert((depth as i8, value));
    *new_entry = (depth as i8, value);
    value
}

/// ValueMax with hash
pub fn value_max_alpha_beta_hash(
    state: &Configuration,
    depth: u8,
    mut alpha: i8,
    beta: i8,
    table: &mut HashMap<String, (i8, i8)>,
) -> i8 {
    if depth == 0 || state.movements().next().is_none() {
        return -state.value();
    }

    let key = state.serialize();
    if table.contains_key(&key) {
        let value = table.get(&key).unwrap();
        if value.0 >= depth as i8 {
            return value.1;
        }
    }

    let mut value: i8 = -64;
    for mvt in state.movements() {
        value = max(
            value,
            value_min_alpha_beta_hash(&state.play(&mvt), depth - 1, alpha, beta, table),
        );
        if value >= beta {
            break;
        }
        alpha = max(alpha, value);
    }
    let new_entry = table.entry(key).or_insert((depth as i8, value));
    *new_entry = (depth as i8, value);
    value
}

/// Pvs algorithm with given maximum number of recursions.
pub struct Pvs(pub u8);

impl fmt::Display for Pvs {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Principal variation search (max level: {})", self.0)
    }
}

impl Strategy for Pvs {
    fn compute_next_move(&mut self, state: &Configuration) -> Option<Movement> {
        decision_principal_variation_search(state, self.0)
    }
}

///Pvs anytime
pub fn pvs_anytime(state: &Configuration) {
    let mut movement = AtomicMove::connect().expect("failed connecting to shmem");
    for depth in 1..100 {
        let chosen_movement = Pvs(depth).compute_next_move(state);
        movement.store(chosen_movement);
    }
}

/// Decision principal variation search
pub fn decision_principal_variation_search(state: &Configuration, depth: u8) -> Option<Movement> {
    let movements: Vec<(i8, Option<Movement>)> = state
        .movements()
        .collect_vec()
        .into_par_iter()
        .map(|mvt| -> (i8, Option<Movement>) {
            let new_value = -principal_variation_search(&state.play(&mvt), depth - 1, -64, 64);
            (new_value, Some(mvt))
        })
        .collect();
    if movements.is_empty() {
        return None;
    }
    movements
        .into_par_iter()
        .max_by_key(|pair| pair.0)
        .unwrap()
        .1
}

/// Principal variation search
pub fn principal_variation_search(state: &Configuration, depth: u8, mut alpha: i8, beta: i8) -> i8 {
    if depth == 0 || state.movements().next().is_none() {
        return -state.value();
    }
    let mut it = state.movements();
    let first: Movement = it.next().unwrap();
    let mut value: i8 = -principal_variation_search(&state.play(&first), depth - 1, -beta, -alpha);
    alpha = max(alpha, value);
    if alpha >= beta {
        return alpha;
    }
    for mvt in it {
        let next = &state.play(&mvt);
        value = -principal_variation_search(next, depth - 1, -alpha - 1, -alpha);
        if alpha < value && value < beta {
            value = -principal_variation_search(next, depth - 1, -beta, -value);
        }
        alpha = max(alpha, value);
        if alpha >= beta {
            break;
        }
    }
    alpha
}
