//! Implementation of the min max algorithm.
use super::Strategy;
use crate::configuration::{Configuration, Movement};
use crate::shmem::AtomicMove;
use itertools::Itertools;
use std::cmp::{max, min};
use std::fmt;

use rayon::prelude::*;

/// Min-Max algorithm with a given recursion depth.
pub struct MinMax(pub u8);

impl Strategy for MinMax {
    fn compute_next_move(&mut self, state: &Configuration) -> Option<Movement> {
        decision_minimax(state, self.0)
    }
}

impl fmt::Display for MinMax {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Min - Max (max level: {})", self.0)
    }
}

/// Min-Max algorithm with a given recursion depth par
pub struct MinMaxPar(pub u8);

impl Strategy for MinMaxPar {
    fn compute_next_move(&mut self, state: &Configuration) -> Option<Movement> {
        decision_minimax_par(state, self.0)
    }
}

impl fmt::Display for MinMaxPar {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Min - Max Par (max level: {})", self.0)
    }
}

/// Anytime min max algorithm.
/// Any time algorithms will compute until a deadline is hit and the process is killed.
/// They are therefore run in another process and communicate through shared memory.
/// This function is intended to be called from blobwar_iterative_deepening.
pub fn min_max_anytime(state: &Configuration) {
    let mut movement = AtomicMove::connect().expect("failed connecting to shmem");
    for depth in 1..100 {
        movement.store(MinMax(depth).compute_next_move(state));
    }
}

/// Decision version par
pub fn decision_minimax_par(state: &Configuration, depth: u8) -> Option<Movement> {
    let movements: Vec<(i8, Option<Movement>)> = state
        .movements()
        .collect_vec()
        .into_par_iter()
        .map(|mvt| -> (i8, Option<Movement>) {
            let new_value = value_min_minimax(&state.play(&mvt), depth - 1);
            (new_value, Some(mvt))
        })
        .collect();
    movements
        .into_par_iter()
        .max_by_key(|pair| pair.0)
        .unwrap()
        .1
}

/// Decision
pub fn decision_minimax(state: &Configuration, depth: u8) -> Option<Movement> {
    let mut value: i8 = -64;
    let mut best_mvt: Option<Movement> = None;
    for mvt in state.movements() {
        let new_value = value_min_minimax(&state.play(&mvt), depth - 1);
        if new_value >= value {
            value = new_value;
            best_mvt = Some(mvt);
        }
    }
    best_mvt
}

/// ValueMin
pub fn value_min_minimax(state: &Configuration, depth: u8) -> i8 {
    if depth == 0 || state.movements().next().is_none() {
        state.value()
    } else {
        let mut value: i8 = 64;
        for mvt in state.movements() {
            value = min(value, value_max_minimax(&state.play(&mvt), depth - 1));
        }
        value
    }
}

/// ValueMax
pub fn value_max_minimax(state: &Configuration, depth: u8) -> i8 {
    if depth == 0 || state.movements().next().is_none() {
        -state.value()
    } else {
        let mut value: i8 = -64;
        for mvt in state.movements() {
            value = max(value, value_min_minimax(&state.play(&mvt), depth - 1));
        }
        value
    }
}
