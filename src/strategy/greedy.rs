//! Dumb greedy algorithm.
use super::Strategy;
use crate::configuration::{Configuration, Movement};
use itertools::Itertools;
use std::fmt;

use rayon::prelude::*;

/// Dumb algorithm.
/// Amongst all possible movements return the one which yields the configuration with the best
/// immediate value.
pub struct Greedy();

impl fmt::Display for Greedy {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Greedy")
    }
}

impl Strategy for Greedy {
    fn compute_next_move(&mut self, state: &Configuration) -> Option<Movement> {
        greedy(state)
    }
}

/// Dumb algorithm.
/// Amongst all possible movements return the one which yields the configuration with the best
/// immediate value.
pub struct GreedyPar();

impl fmt::Display for GreedyPar {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Greedy Par")
    }
}

impl Strategy for GreedyPar {
    fn compute_next_move(&mut self, state: &Configuration) -> Option<Movement> {
        greedy_par(state)
    }
}

/// Greedy seq
pub fn greedy(state: &Configuration) -> Option<Movement> {
    let mut score = -64;
    let mut best_mvt: Option<Movement> = None;
    for mvt in state.movements() {
        let new_state = state.play(&mvt);
        let value = new_state.value();
        if value >= score {
            score = value;
            best_mvt = Some(mvt);
        }
    }
    best_mvt
}

/// Greedy par
pub fn greedy_par(state: &Configuration) -> Option<Movement> {
    let mut sorted_movements = state.movements().collect_vec();
    sorted_movements.par_sort_by_key(|mvt| -state.play(mvt).value());
    Some(sorted_movements[0])
}
