This is an implementation of several algorithms to play blobwar in Rust.

Blobwar is a game opposing two players playing on a chess board.
Each player starts with a blob in two corners.
On each turn, a player can choose to either duplicate a blob to one of the 8 neighboring squares, or to jump with a blob to two squares away.
If the player chooses to jump, the blob will disappear from the starting square.
After a turn is played, every blob surrounding the arriving square will change to the player who just played's colour.

The goal is to finish the game with more blobs than your opponent.
The game finishes when either the board is filled, either one player is completely eliminated.
If a player cannot play, they must skip their turn.

The program is set to make a human play against the PvsSort algorithm, but this can be easily changed in main.rs.
Other available algorithms are included in the 4th line of main.rs, they are implemented in src/strategy/.
Most algorithms are programmed to be optimised when using a time limit, so if they are played given unlimited time, they are often equally matched.

To play, you must first input a starting square, followed by a finishing square.
Inputs are as follow:
line_number column_number
ex: 0 7
Please follow this format to avoid errors.

To launch the game (requires Rust):
cargo run --bin blobwar

The other binaries are for playing against other programmer's algorithms, so usage is not detailed here.

pictures/ contains pictures of the game running to give you an idea of how it works without having to install it.
Enjoy!